#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int contadorAvl = 0;

typedef struct noAvl {
    struct noAvl * pai;
    struct noAvl * esquerda;
    struct noAvl * direita;
    int valor;
} NoAvl;

typedef struct arvoreAvl {
    struct noAvl * raiz;
} ArvoreAvl;

void balanceamentoAvl(ArvoreAvl* , NoAvl* );
int alturaAvl(NoAvl* );
int fbAvl(NoAvl* );
NoAvl * rsdAvl(ArvoreAvl*, NoAvl* );
NoAvl * rseAvl(ArvoreAvl*, NoAvl* );
NoAvl * rddAvl(ArvoreAvl*, NoAvl* );
NoAvl * rdeAvl(ArvoreAvl*, NoAvl* );
NoAvl * adicionarNoAvl(NoAvl* , int);
NoAvl * localizarAvl(NoAvl* , int);
int localizar2Avl(NoAvl* , int);
NoAvl* adicionarAvl(ArvoreAvl* , int);

ArvoreAvl * criarAvl() {
    ArvoreAvl * arvore = malloc(sizeof(ArvoreAvl));
    arvore -> raiz = NULL;

    return arvore;
}

int vaziaAvl(ArvoreAvl * arvore) {
    return arvore -> raiz == NULL;
}

void removerAvl(ArvoreAvl * arvore, NoAvl * no) {
    if (no -> esquerda != NULL) {
        removerAvl(arvore, no -> esquerda);
    }

    if (no -> direita != NULL) {
        removerAvl(arvore, no -> direita);
    }

    if (no -> pai == NULL) {
        arvore -> raiz = NULL;
    } else {
        if (no -> pai -> esquerda == no) {
            no -> pai -> esquerda = NULL;
        } else {
            no -> pai -> direita = NULL;
        }
    }

    free(no);
}

void percorrerProfundidadeInOrderAvl(NoAvl * no, void( * callback)(int)) {
    if (no != NULL) {
        percorrerProfundidadeInOrderAvl(no -> esquerda, callback);
        callback(no -> valor);
        percorrerProfundidadeInOrderAvl(no -> direita, callback);
    }
}

void percorrerProfundidadePreOrderAvl(NoAvl * no, void( * callback)(int)) {
    if (no != NULL) {
        callback(no -> valor);
        percorrerProfundidadePreOrderAvl(no -> esquerda, callback);
        percorrerProfundidadePreOrderAvl(no -> direita, callback);
    }
}

void percorrerProfundidadePosOrderAvl(NoAvl * no, void(callback)(int)) {
    if (no != NULL) {
        percorrerProfundidadePosOrderAvl(no -> esquerda, callback);
        percorrerProfundidadePosOrderAvl(no -> direita, callback);
        callback(no -> valor);
    }
}

void visitarAvl(int valor) {
    printf("%d ", valor);
}

int alturaAvl(NoAvl * no) {
    int esquerda = 0, direita = 0;

    if (no -> esquerda != NULL) {
        esquerda = alturaAvl(no -> esquerda) + 1;
    }

    if (no -> direita != NULL) {
        direita = alturaAvl(no -> direita) + 1;
    }

    return esquerda > direita ? esquerda : direita; //max(esquerda,direita)
}

int main() {
    FILE *saidaAvl;
    int i, j = 1, nVal, ctrl, k, nTest = 0;
    int vetMedia[10][1000];
    int vetVal[1000];
    saidaAvl = fopen("arvore_avl_caso_medio.txt", "w");

    do{
        for (j = 1; j <= 1000; j++) {
            ArvoreAvl* a = criarAvl();
            srand(time(NULL));

            for (i = 1; i <= j; i++) {
                if (i > 1) {
                    do {
                        ctrl = 1;
                        nVal = rand() % 1000;
                        if (localizar2Avl(a -> raiz, nVal) == 1) {
                            ctrl = 0;
                        }

                    } while (ctrl == 0);
                }
                adicionarAvl(a, nVal);
            }

            vetMedia[nTest][j - 1] = contadorAvl;
            contadorAvl = 0;
        }
        nTest++;
    } while (nTest < 10);
    for (i = 0; i < 1000; i++){
        for(j = 0; j < 10; j++){
            if(j == 9){
               vetVal[i] += vetMedia[j][i];
               vetVal[i] = (vetVal[i]/10); 
            }else{
                vetVal[i] += vetMedia[j][i];
            }
        }
    }

    for (i = 0; i < 1000; i++){
        printf("%i", i);
        printf(";");
        printf("%i\n", vetVal[i]);
        //fprintf(saidaAvl, "%i", i);
        //fprintf(saidaAvl, ";");
        //fprintf(saidaAvl, "%i\n", vetVal[i]);
    }
    printf("teste");
}

NoAvl * adicionarNoAvl(NoAvl * no, int valor) {
    if (valor > no -> valor) {
        if (no -> direita == NULL) {
            contadorAvl++;
            NoAvl * novo = malloc(sizeof(NoAvl));

            novo -> direita = NULL;
            novo -> esquerda = NULL;
            novo -> pai = NULL;

            novo -> valor = valor;
            novo -> pai = no;

            no -> direita = novo;

            return novo;
        } else {
            return adicionarNoAvl(no -> direita, valor);
        }
    } else {
        if (no -> esquerda == NULL) {
            contadorAvl++;
            NoAvl * novo = malloc(sizeof(NoAvl));

            novo -> direita = NULL;
            novo -> esquerda = NULL;
            novo -> pai = NULL;

            novo -> valor = valor;
            novo -> pai = no;

            no -> esquerda = novo;

            return novo;
        } else {
            return adicionarNoAvl(no -> esquerda, valor);
        }
    }
}

NoAvl * adicionarAvl(ArvoreAvl * arvore, int valor) {
    if (arvore -> raiz == NULL) {
        contadorAvl++;
        NoAvl * novo = malloc(sizeof(NoAvl));
        novo -> valor = valor;

        novo -> direita = NULL;
        novo -> esquerda = NULL;
        novo -> pai = NULL;

        arvore -> raiz = novo;

        return novo;
    } else {
        NoAvl * no = adicionarNoAvl(arvore -> raiz, valor);
        balanceamentoAvl(arvore, no);

        return no;
    }
}

int localizar2Avl(NoAvl * no, int valor) {

    if (no -> valor == valor) {
        return 1;
    } else {
        if (valor < no -> valor) {
            if (no -> esquerda != NULL) {
                return localizar2Avl(no -> esquerda, valor);
            }
        } else {
            if (no -> direita != NULL) {
                return localizar2Avl(no -> direita, valor);
            }
        }
    }

    return 0;
}

void balanceamentoAvl(ArvoreAvl * arvore, NoAvl * no) {
    while (no != NULL) {
        contadorAvl++;
        int fator = fbAvl(no);

        if (fator > 1) { //arvore mais pesada para esquerda
            //rotaciona para a direita
            if (fbAvl(no -> esquerda) > 0) {
                rsdAvl(arvore, no); //rotacao simples a direita
            } else {
                rddAvl(arvore, no); //rotacao dupla a direita
            }
        } else if (fator < -1) { //arvore mais pesada para a direita
            //rotaciona para a esquerda
            if (fbAvl(no -> direita) < 0) {
                rseAvl(arvore, no); //rotacao simples a esquerda
            } else {
                rdeAvl(arvore, no); //rotacao dupla a esquerda
            }
        }

        no = no -> pai;
    }
}

int fbAvl(NoAvl * no) {
    contadorAvl++;
    int esquerda = 0, direita = 0;

    if (no -> esquerda != NULL) {
        esquerda = alturaAvl(no -> esquerda) + 1;
    }

    if (no -> direita != NULL) {
        direita = alturaAvl(no -> direita) + 1;
    }

    return esquerda - direita;
}

NoAvl * rseAvl(ArvoreAvl * arvore, NoAvl * no) {
    contadorAvl++;
    NoAvl * pai = no -> pai;
    NoAvl * direita = no -> direita;

    no -> direita = direita -> esquerda;
    no -> pai = direita;

    direita -> esquerda = no;
    direita -> pai = pai;

    if (pai == NULL) {
        arvore -> raiz = direita;
    } else {
        if (pai -> esquerda == no) {
            pai -> esquerda = direita;
        } else {
            pai -> direita = direita;
        }
    }

    return direita;
}

NoAvl * rsdAvl(ArvoreAvl * arvore, NoAvl * no) {
    contadorAvl++;
    NoAvl * pai = no -> pai;
    NoAvl * esquerda = no -> esquerda;

    no -> esquerda = esquerda -> direita;
    no -> pai = esquerda;

    esquerda -> direita = no;
    esquerda -> pai = pai;

    if (pai == NULL) {
        arvore -> raiz = esquerda;
    } else {
        if (pai -> esquerda == no) {
            pai -> esquerda = esquerda;
        } else {
            pai -> direita = esquerda;
        }
    }

    return esquerda;
}

NoAvl * rdeAvl(ArvoreAvl * arvore, NoAvl * no) {
    contadorAvl++;
    no -> direita = rsdAvl(arvore, no -> direita);
    return rseAvl(arvore, no);
}

NoAvl * rddAvl(ArvoreAvl * arvore, NoAvl * no) {
    contadorAvl++;
    no -> esquerda = rseAvl(arvore, no -> esquerda);
    return rsdAvl(arvore, no);
}
