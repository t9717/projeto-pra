#include <stdio.h>
#include <stdlib.h>

int contadorB;

typedef struct no {
    int total;
    int* chaves;
    struct no** filhos;
    struct no* pai; 
} NoB;

typedef struct arvoreB {
    NoB* raiz;
    int ordem;
} ArvoreB;

ArvoreB* criaArvoreB(int);
NoB* criaNoB(ArvoreB*);
void percorreArvoreB(NoB*);
int pesquisaBinariaB(NoB*, int);
int localizaChaveB(ArvoreB*, int);
NoB* localizaNoB(ArvoreB*, int);
void adicionaChaveNoB(NoB*, NoB*, int);
int transbordoB(ArvoreB*, NoB*);
NoB* divideNoB(ArvoreB*, NoB*);
void adicionaChaveRecursivoB(ArvoreB*, NoB*, NoB*, int);
void adicionaChaveB(ArvoreB*, int);

int main() {
	
    FILE *saidaB;
    int *vetor;
    int i, j = 1, n, k;
    saidaB = fopen("arvore_b_caso_1.txt", "w");
	
    for(j = 1; j <= 1000; j++ ) { 
		ArvoreB* a = criaArvoreB(1);

		for (i = 1; i <= j; i++){	
			adicionaChaveB(a, i);
		}
		fprintf(saidaB, "%i", j);
		fprintf(saidaB, ",");
		fprintf(saidaB, "%i\n", contadorB);
		contadorB = 0;
	} 
}



ArvoreB* criaArvoreB(int ordem) {
    ArvoreB* a = malloc(sizeof(ArvoreB));
    a->ordem = ordem;
    a->raiz = criaNoB(a);

    return a;
}

NoB* criaNoB(ArvoreB* arvore) {
    int max = arvore->ordem * 2;
    int i;
    NoB* no = malloc(sizeof(NoB));

    no->pai = NULL;
        
    no->chaves = malloc(sizeof(int) * (max + 1));
    no->filhos = malloc(sizeof(NoB) * (max + 2));
    no->total = 0;

    for ( i = 0; i < max + 2; i++)
        no->filhos[i] = NULL;

    return no;
}

void percorreArvoreB(NoB* no) {
	int i;
    if (no != NULL) {
        for ( i = 0; i < no->total; i++){
            percorreArvoreB(no->filhos[i]); //visita o filho a esquerda
            
            printf("%d ",no->chaves[i]);
        }

        percorreArvoreB(no->filhos[no->total]); //visita ultimo filho (direita)
    }
}

int pesquisaBinariaB(NoB* no, int chave) {
    int inicio = 0, fim = no->total - 1, meio;		
    
    while (inicio <= fim) {	
        
        meio = (inicio + fim) / 2;
        
        if (no->chaves[meio] == chave) {	
		    return meio; //encontrou	
        } else if (no->chaves[meio] > chave) {
                fim	= meio - 1;	
        } else {
            inicio = meio + 1;
        }
    }
    return inicio; //nao encontrou	
}

int localizaChaveB(ArvoreB* arvore, int chave) {	
    NoB *no = arvore->raiz;
    
    while (no != NULL) {
        int i = pesquisaBinariaB(no, chave);

        if (i < no->total && no->chaves[i] == chave) {
            return 1; //encontrou
        } else {
            no = no->filhos[i];
        }
    }

    return 0; //nao encontrou	
}

NoB* localizaNoB(ArvoreB* arvore, int chave) {	
    NoB *no = arvore->raiz;
    
    while (no != NULL) {
        contadorB++;

        int i = pesquisaBinariaB(no, chave);

        if (no->filhos[i] == NULL)
            return no; //encontrou no
        else
            no = no->filhos[i];
    }

    return NULL; //nao encontrou nenhum no
}

void adicionaChaveNoB(NoB* no, NoB* novo, int chave) {
    int i = pesquisaBinariaB(no, chave);
    int j;
    contadorB++;

    for ( j = no->total - 1; j >= i; j--) {
        no->chaves[j + 1] = no->chaves[j];
        no->filhos[j + 2] = no->filhos[j + 1];
    }
    
    no->chaves[i] = chave;
    no->filhos[i + 1] = novo;

    no->total++;
}

int transbordoB(ArvoreB* arvore, NoB* no) {
    contadorB++;
    
    return no->total > arvore->ordem * 2;
}

NoB* divideNoB(ArvoreB* arvore, NoB* no) {
    int meio = no->total / 2;
    NoB* novo = criaNoB(arvore);
    novo->pai = no->pai;
	int i;
    contadorB++;
    
    for ( i = meio + 1; i < no->total; i++) {
        novo->filhos[novo->total] = no->filhos[i];
        novo->chaves[novo->total] = no->chaves[i];
        if (novo->filhos[novo->total] != NULL) novo->filhos[novo->total]->pai = novo;
        
        novo->total++;
    }

    novo->filhos[novo->total] = no->filhos[no->total];
    if (novo->filhos[novo->total] != NULL) novo->filhos[novo->total]->pai = novo;    
    no->total = meio;
    return novo;
}

void adicionaChaveRecursivoB(ArvoreB* arvore, NoB* no, NoB* novo, int chave) {
    contadorB++;
    
    adicionaChaveNoB(no, novo, chave);
    
    if (transbordoB(arvore, no)) {
        int promovido = no->chaves[arvore->ordem]; 
        NoB* novo = divideNoB(arvore, no);

        if (no->pai == NULL) {
            contadorB++;
            
            NoB* pai = criaNoB(arvore);            
            pai->filhos[0] = no;
            adicionaChaveNoB(pai, novo, promovido);
            
            no->pai = pai;
            novo->pai = pai;
            arvore->raiz = pai;
        } else
            adicionaChaveRecursivoB(arvore, no->pai, novo, promovido);
    }
}

void adicionaChaveB(ArvoreB* arvore, int chave) {
    NoB* no = localizaNoB(arvore, chave);

    adicionaChaveRecursivoB(arvore, no, NULL, chave);
}
