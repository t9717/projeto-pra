#include <stdlib.h>
#include <stdio.h>

int contadorRn = 0;
enum coloracao { Vermelho, Preto };
typedef enum coloracao Cor;

typedef struct no {
    struct no* pai;
    struct no* esquerda;
    struct no* direita;
    Cor cor;
    int valor;
} NoRn;

typedef struct arvore {
    struct no* raiz;
    struct no* nulo; 
} ArvoreRn;

NoRn* criarNoRn(ArvoreRn*, NoRn*, int);
void balancearRn(ArvoreRn*, NoRn*);
void rotacionarEsquerdaRn(ArvoreRn*, NoRn*);
void rotacionarDireitaRn(ArvoreRn*, NoRn*);

ArvoreRn* criarRn();
int vaziaRn(ArvoreRn*);
NoRn* adicionarRn(ArvoreRn*, int);
NoRn* localizarRn(ArvoreRn* arvore, int valor);


int main() {
    //Arvore* a = criar();
  
    FILE *saidaRn;
    saidaRn = fopen("arvore_rn_caso_111.txt", "w");	
	int i, j = 1, n, ctrl;
	
	for(j = 1; j<= 1000; j++ ){
		ArvoreRn* a = criarRn(); 	
		 	
		for (i = 1; i<=j; i++){
			adicionarRn(a, i);		
		}
		fprintf(saidaRn, "%i", j);
		fprintf(saidaRn, ",");
		fprintf(saidaRn, "%i\n", contadorRn);
		
        contadorRn = 0;
	}

}


ArvoreRn* criarRn() {
    ArvoreRn *arvore = malloc(sizeof(ArvoreRn));
    arvore->nulo = NULL;
    arvore->raiz = NULL;

    arvore->nulo = criarNoRn(arvore, NULL, 0);
    arvore->nulo->cor = Preto;

    return arvore;
}

int vaziaRn(ArvoreRn* arvore) {
    return arvore->raiz == NULL;
}

NoRn* criarNoRn(ArvoreRn* arvore, NoRn* pai, int valor) {
	contadorRn++;
    NoRn* no = malloc(sizeof(NoRn));

    no->pai = pai;    
    no->valor = valor;
    no->direita = arvore->nulo;
    no->esquerda = arvore->nulo;

    return no;
}

NoRn* adicionarNoRn(ArvoreRn* arvore, NoRn* no, int valor) {
	contadorRn++;
    if (valor > no->valor) {
        if (no->direita == arvore->nulo) {
            no->direita = criarNoRn(arvore, no, valor);     
            no->direita->cor = Vermelho;       
        		
            return no->direita;
        } else {
            return adicionarNoRn(arvore, no->direita, valor);
        }
    } else {
        if (no->esquerda == arvore->nulo) {
            no->esquerda = criarNoRn(arvore, no, valor);
            no->esquerda->cor = Vermelho;
            
            return no->esquerda;
        } else {
            return adicionarNoRn(arvore, no->esquerda, valor);
        }
    }
}

NoRn* adicionarRn(ArvoreRn* arvore, int valor) {
	contadorRn++;
    if (vaziaRn(arvore)) {
        arvore->raiz = criarNoRn(arvore, arvore->nulo, valor);
        arvore->raiz->cor = Preto;
        	
        return arvore->raiz;
    } else {
        NoRn* no = adicionarNoRn(arvore, arvore->raiz, valor);
        balancearRn(arvore, no);
        
        return no;
    }
}

NoRn* localizarRn(ArvoreRn* arvore, int valor) {
    if (!vaziaRn(arvore)) {
        NoRn* no = arvore->raiz;

        while (no != arvore->nulo) {
            if (no->valor == valor) {
                return no;
            } else {
                no = valor < no->valor ? no->esquerda : no->direita;
            }
        }
    }

    return NULL;
}

void percorrerProfundidadeInOrderRn(ArvoreRn* arvore, NoRn* no, void (*callback)(int)) {
    if (no != arvore->nulo) {
        
        percorrerProfundidadeInOrderRn(arvore, no->esquerda,callback);
        callback(no->valor);
        percorrerProfundidadeInOrderRn(arvore, no->direita,callback);
    }
}

void percorrerProfundidadePreOrderRn(ArvoreRn* arvore, NoRn* no, void (*callback)(int)) {
    if (no != arvore->nulo) {
        callback(no->valor);
        percorrerProfundidadePreOrderRn(arvore, no->esquerda,callback);
        percorrerProfundidadePreOrderRn(arvore, no->direita,callback);
    }
}

void percorrerProfundidadePosOrderRn(ArvoreRn* arvore, NoRn* no, void (callback)(int)) {
    if (no != arvore->nulo) {
        percorrerProfundidadePosOrderRn(arvore, no->esquerda,callback);
        percorrerProfundidadePosOrderRn(arvore, no->direita,callback);
        callback(no->valor);
    }
}

void visitarRn(int valor){
    printf("%d ", valor);
}

void balancearRn(ArvoreRn* arvore, NoRn* no) {
    while (no->pai->cor == Vermelho) {
    	contadorRn++;
        if (no->pai == no->pai->pai->esquerda) {
            NoRn *tio = no->pai->pai->direita;
            
            if (tio->cor == Vermelho) {
                tio->cor = Preto; //Caso 1
                no->pai->cor = Preto; 

                no->pai->pai->cor = Vermelho; //Caso 1
                no = no->pai->pai; //Caso 1
            } else {
                if (no == no->pai->direita) {
                    no = no->pai; //Caso 2
                    rotacionarEsquerdaRn(arvore, no); //Caso 2
                } else {
                    no->pai->cor = Preto; 
                    no->pai->pai->cor = Vermelho; //Caso 3
                    rotacionarDireitaRn(arvore, no->pai->pai); //Caso 3
                }
            }
        } else {
            NoRn *tio = no->pai->pai->esquerda;
            
            if (tio->cor == Vermelho) {
                tio->cor = Preto; //Caso 1
                no->pai->cor = Preto; 

                no->pai->pai->cor = Vermelho; //Caso 1
                no = no->pai->pai; //Caso 1
            } else {
                if (no == no->pai->esquerda) {
                    no = no->pai; //Caso 2
                    rotacionarDireitaRn(arvore, no); //Caso 2
                } else {
                    no->pai->cor = Preto; 
                    no->pai->pai->cor = Vermelho; //Caso 3
                    rotacionarEsquerdaRn(arvore, no->pai->pai); //Caso 3
                }
            }
        }
    }
    arvore->raiz->cor = Preto; //Conserta poss�vel quebra regra 2
}

void rotacionarEsquerdaRn(ArvoreRn* arvore, NoRn* no) {
    NoRn* direita = no->direita;
    no->direita = direita->esquerda; 
	contadorRn++;

    if (direita->esquerda != arvore->nulo) {
        direita->esquerda->pai = no;
    }

    direita->pai = no->pai;
    
    if (no->pai == arvore->nulo) {
        arvore->raiz = direita;
    } else if (no == no->pai->esquerda) {
        no->pai->esquerda = direita;
    } else {
        no->pai->direita = direita;
    }

    direita->esquerda = no;
    no->pai = direita;
}

void rotacionarDireitaRn(ArvoreRn* arvore, NoRn* no) {
    NoRn* esquerda = no->esquerda;
    no->esquerda = esquerda->direita;
    contadorRn++;
    
    if (esquerda->direita != arvore->nulo) {
        esquerda->direita->pai = no;
    }
    
    esquerda->pai = no->pai;
    
    if (no->pai == arvore->nulo) {
        arvore->raiz = esquerda;
    } else if (no == no->pai->esquerda) {
        no->pai->esquerda = esquerda;
    } else {
        no->pai->direita = esquerda;
    }
    
    esquerda->direita = no;
    no->pai = esquerda;
}
